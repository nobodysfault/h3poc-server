const queryOverpass = require('query-overpass');
const connectionOptions = require('./db-connect');
const mysql = require('mysql');
const h3 = require('h3-js');

const area = '45.2,7,49.41,10';
const amenity = 'bar';

const connection = mysql.createConnection(connectionOptions);
connection.connect();

queryOverpass(`[out:json];node(${area})[amenity=${amenity}];out;`, function (err, geoJson) {
    if (err) {
        console.error(err);
        return;
    }
    let features = geoJson['features'];
    let items = features.map(feature => {
        let lat = feature['geometry']['coordinates'][0];
        let lon = feature['geometry']['coordinates'][1];
        let h3index = h3.geoToH3(lat, lon, 15);
        return {
            name: feature['properties']['name'] || "Unknown",
            lat: lat,
            lon: lon,
            h3index: BigInt(`0x${h3index}`)
        }
    });
    let itemValues = items.map(item => Object.values(item));
    connection.query('INSERT INTO places (name, lat, lon, h3index) VALUES ?', [itemValues], function (err, results) {
        if (err) {
            console.log(err);
        }
        connection.end();
    });
}, {flatProperties: true});
