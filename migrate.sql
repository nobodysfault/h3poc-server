CREATE TABLE places (
  id         INT          NOT NULL AUTO_INCREMENT,
  name       VARCHAR(255) NOT NULL,
  lat        DOUBLE       NOT NULL,
  lon        DOUBLE       NOT NULL,
  h3index    BIGINT       NOT NULL,
  h3index_15 BIGINT                GENERATED ALWAYS AS (h3index) VIRTUAL NOT NULL,
  h3index_14 BIGINT                GENERATED ALWAYS AS (
                                     h3index & ((~(cast(0b1111 AS UNSIGNED) << 0x34)) | (cast(14 AS UNSIGNED) << 0x34))
                                     & (~(cast(7 AS UNSIGNED)) << 0)) VIRTUAL NOT NULL,
  h3index_13 BIGINT                GENERATED ALWAYS AS (
                                     h3index & ((~(cast(0b1111 AS UNSIGNED) << 0x34)) | (cast(13 AS UNSIGNED) << 0x34))
                                     & (~(cast(7 AS UNSIGNED)) << 0x3)) VIRTUAL NOT NULL,
  h3index_12 BIGINT                GENERATED ALWAYS AS (
                                     h3index & ((~(cast(0b1111 AS UNSIGNED) << 0x34)) | (cast(12 AS UNSIGNED) << 0x34))
                                     & (~(cast(7 AS UNSIGNED)) << 0x6)) VIRTUAL NOT NULL,
  h3index_11 BIGINT                GENERATED ALWAYS AS (
                                     h3index & ((~(cast(0b1111 AS UNSIGNED) << 0x34)) | (cast(11 AS UNSIGNED) << 0x34))
                                     & (~(cast(7 AS UNSIGNED)) << 0x9)) VIRTUAL NOT NULL,
  h3index_10 BIGINT                GENERATED ALWAYS AS (
                                     h3index & ((~(cast(0b1111 AS UNSIGNED) << 0x34)) | (cast(10 AS UNSIGNED) << 0x34))
                                     & (~(cast(7 AS UNSIGNED)) << 0x0C)) VIRTUAL NOT NULL,
  h3index_9  BIGINT                GENERATED ALWAYS AS (
                                     h3index & ((~(cast(0b1111 AS UNSIGNED) << 0x34)) | (cast(9 AS UNSIGNED) << 0x34))
                                     & (~(cast(7 AS UNSIGNED)) << 0x0F)) VIRTUAL NOT NULL,
  h3index_8  BIGINT                GENERATED ALWAYS AS (
                                     h3index & ((~(cast(0b1111 AS UNSIGNED) << 0x34)) | (cast(8 AS UNSIGNED) << 0x34))
                                     & (~(cast(7 AS UNSIGNED)) << 0x12)) VIRTUAL NOT NULL,
  h3index_7  BIGINT                GENERATED ALWAYS AS (
                                     h3index & ((~(cast(0b1111 AS UNSIGNED) << 0x34)) | (cast(7 AS UNSIGNED) << 0x34))
                                     & (~(cast(7 AS UNSIGNED)) << 0x15)) VIRTUAL NOT NULL,
  h3index_6  BIGINT                GENERATED ALWAYS AS (
                                     h3index & ((~(cast(0b1111 AS UNSIGNED) << 0x34)) | (cast(6 AS UNSIGNED) << 0x34))
                                     & (~(cast(7 AS UNSIGNED)) << 0x18)) VIRTUAL NOT NULL,
  h3index_5  BIGINT                GENERATED ALWAYS AS (
                                     h3index & ((~(cast(0b1111 AS UNSIGNED) << 0x34)) | (cast(5 AS UNSIGNED) << 0x34))
                                     & (~(cast(7 AS UNSIGNED)) << 0x1B)) VIRTUAL NOT NULL,
  h3index_4  BIGINT                GENERATED ALWAYS AS (
                                     h3index & ((~(cast(0b1111 AS UNSIGNED) << 0x34)) | (cast(4 AS UNSIGNED) << 0x34))
                                     & (~(cast(7 AS UNSIGNED)) << 0x1E)) VIRTUAL NOT NULL,
  h3index_3  BIGINT                GENERATED ALWAYS AS (
                                     h3index & ((~(cast(0b1111 AS UNSIGNED) << 0x34)) | (cast(3 AS UNSIGNED) << 0x34))
                                     & (~(cast(7 AS UNSIGNED)) << 0x21)) VIRTUAL NOT NULL,
  h3index_2  BIGINT                GENERATED ALWAYS AS (
                                     h3index & ((~(cast(0b1111 AS UNSIGNED) << 0x34)) | (cast(2 AS UNSIGNED) << 0x34))
                                     & (~(cast(7 AS UNSIGNED)) << 0x24)) VIRTUAL NOT NULL,
  h3index_1  BIGINT                GENERATED ALWAYS AS (
                                     h3index & ((~(cast(0b1111 AS UNSIGNED) << 0x34)) | (cast(1 AS UNSIGNED) << 0x34))
                                     & (~(cast(7 AS UNSIGNED)) << 0x27)) VIRTUAL NOT NULL,
  h3index_0  BIGINT                GENERATED ALWAYS AS (
                                     h3index & ((~(cast(0b1111 AS UNSIGNED) << 0x34)) | (cast(0 AS UNSIGNED) << 0x34))
                                     & (~(cast(7 AS UNSIGNED)) << 0x2A)) VIRTUAL NOT NULL,
  PRIMARY KEY (id),
  INDEX i15 (h3index_15),
  INDEX i14 (h3index_14),
  INDEX i13 (h3index_13),
  INDEX i12 (h3index_12),
  INDEX i11 (h3index_11),
  INDEX i10 (h3index_10),
  INDEX i9 (h3index_9),
  INDEX i8 (h3index_8),
  INDEX i7 (h3index_7),
  INDEX i6 (h3index_6),
  INDEX i5 (h3index_5),
  INDEX i4 (h3index_4),
  INDEX i3 (h3index_3),
  INDEX i2 (h3index_2),
  INDEX i1 (h3index_1),
  INDEX i0 (h3index_0)
)
  ENGINE = InnoDB;
