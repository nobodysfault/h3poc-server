const h3 = require('h3-js');
const pool = require('./database');
const fastify = require('fastify')({
    logger: true
});

const resolutionToFieldMap = resolution => `h3index_${resolution}`;
const getCellId = cell => BigInt(`0x${cell}`);
const getFieldName = cell => resolutionToFieldMap(h3.h3GetResolution(cell));

fastify.get('/count', async (request, reply) => {
    let cells = request.query.cells.split(',');
    return Promise.all(cells.map(cell => {
        let cellId = getCellId(cell);
        let fieldName = getFieldName(cell);
        return pool.query(
            'SELECT ? as cell, COUNT(*) as count FROM places WHERE ?? = ?',
            [cell, fieldName, cellId]
        ).then(rows => rows[0]);
    }));
});

fastify.get('/cell/:cell', async (request, reply) => {
    let cell = request.params.cell;
    let cellId = getCellId(cell);
    let fieldName = getFieldName(cell);
    return pool.query(
        'SELECT id, name, lat, lon FROM places WHERE ?? = ?',
        [fieldName, cellId]
    );
});

fastify.listen(8000, (err, address) => {
    if (err) {
        throw err;
    }
    fastify.log.info(`server listening on ${address}`);
});
